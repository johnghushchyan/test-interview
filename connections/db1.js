const mongoose = require('mongoose');
const { infoLog, errorLog } = require('../utils/logs');

mongoose.Promise = Promise;

const url = process.env.MONGO_DB1;

const options = {
  maxPoolSize: 50,
  wtimeoutMS: 2500,
  useNewUrlParser: true,
};
mongoose.connect(url, options);

mongoose.connection.on('disconnected', (err) => {
  errorLog(err);
  infoLog(`Reconnecting to: ${process.env.MONGO_DB1}`);
  setTimeout(() => {
    mongoose.connect(url, options);
  }, 5000);
});
mongoose.connection.on('open', () => {
  infoLog(`Connected to ${url}`);
});

const connBase = mongoose.connection;

module.exports = {
  connBase,
};
