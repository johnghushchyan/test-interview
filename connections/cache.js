const redis = require('redis');
const { infoLog } = require('../utils/logs');

const redisClient = redis.createClient();

(async () => {
  await redisClient.connect();
})();

redisClient.on('connect', () => {
  infoLog('Connected to Redis');
});

redisClient.on('error', (err) => {
  infoLog(err, 'redis');
});

module.exports = {
  redisClient,
};
