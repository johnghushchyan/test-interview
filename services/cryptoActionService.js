const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '.env') });
const { CryptoCompareDbModel } = require('../schemes/cryptoCompare');
const { redisClient } = require('../connections/cache');

class CryptoActionsService {
  static async storeCrypto() {
    try {
      const getData = await redisClient.get('crypto');

      if (!getData) {
        return null;
      }
      const { RAW, DISPLAY } = JSON.parse(getData);

      await Promise.all([
        CryptoCompareDbModel.create({ RAW, DISPLAY }),
        redisClient.del('crypto'),
      ]);

      return {
        status: 'Ok',
      };
    } catch (e) {
      return null;
    }
  }
}

module.exports = {
  CryptoActionsService,
};
