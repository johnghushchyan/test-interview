const request = require('request');
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '.env') });
const { BadRequestException } = require('../utils/error');
const { redisClient } = require('../connections/cache');
const { CryptoCompareDbModel } = require('../schemes/cryptoCompare');

class CryptoActionsController {
  static async cryptoBusinessLogic(req, res, next) {
    try {
      const { query } = req;

      if (!query) {
        throw new BadRequestException('query is not provided');
      }

      const { fsyms, tsyms } = query;
      const fsy = fsyms.split(',');
      const tsy = tsyms.split(',');

      if (fsy.length === 0 || tsy.length === 0) {
        throw new BadRequestException('query is not provided');
      }

      const url = `${process.env.CRYPTO_PATH}?fsyms=${fsyms}&tsyms=${tsyms}`;
      const data = await new Promise((resolve, reject) => {
        request.get(url, (error, response, body) => {
          if (error) {
            return reject(error);
          }
          const cryptoStructureJSON = JSON.parse(body);
          return resolve(cryptoStructureJSON);
        });
      });
      await redisClient.set('crypto', JSON.stringify(data));

      return {
        data,
      };
    } catch (e) {
      return next(e);
    }
  }
}

module.exports = {
  CryptoActionsController,
};
