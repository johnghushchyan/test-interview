const app = require('express')();
const path = require('path');
// eslint-disable-next-line import/no-extraneous-dependencies
const nodeCron = require('node-cron');
require('dotenv').config({ path: path.resolve(__dirname, '.env') });
const logger = require('morgan');
const winston = require('winston');
const bodyParser = require('body-parser');
const { CryptoActionsService } = require('./services/cryptoActionService');
const api = require('./routes/api');
require('dotenv').config({ path: path.resolve(__dirname, '.env') });
const { errorLog } = require('./utils/logs');

require('./connections/db1');
require('./connections/cache');

/* Express middleware */
app.use(bodyParser.urlencoded({ extended: false, type: 'application/x-www-form-urlencoded' }));
app.use(bodyParser.text({ type: 'application/x-www-form-urlencoded', limit: '6mb' }));
app.use(logger('dev'));
app.use((req, res, next) => {
  if (req.method === 'OPTIONS') {
    if (req.headers['access-control-request-headers']) {
      res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
    }
    return res.send();
  }
  next();
});

app.use('/api', api);

nodeCron.schedule('0 */55 * * * *', async () => {
  const { status } = await CryptoActionsService.storeCrypto();

  if (!status) {
    errorLog(`${(new Date()).toUTCString()} cannot save db:`);
  }
});

/* Production Error Handler */
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  const exception = err.exception || 'Default';
  if (err.content) {
    return res.json({ error: err.content || {}, exception });
  }
  if (err.code && err.code.toString() === '11000') {
    return res.status(409).json({ message: 'duplicate key error, please be sure you sent request once at a time', error: {}, exception });
  }
  res.json({ message: err.message, error: {}, exception });
});

/* Application Listening On PORT */
app.listen(
  process.env.SERVER_PORT,
  process.env.SERVER_HOSTNAME,
  winston.log('info', `Node.js server is running at http://${process.env.SERVER_HOSTNAME}:${process.env.SERVER_PORT} 
    in ${process.env.NODE_ENV} mode with process id ${process.pid}`),
);

process.on('uncaughtException', (err) => {
  errorLog(`${(new Date()).toUTCString()} uncaughtException:`, err.message);
  errorLog(err);
  process.exit(1);
});
