const router = require('express').Router();
const winston = require('winston');
const { CryptoActionsController } = require('../controller/cryptoActions');

/**
 * CryptoCurrencyAPI
 */

router.get('/crypto', async (req, res, next) => {
  try {
    res.send(await CryptoActionsController.cryptoBusinessLogic(req));
  } catch (err) { winston.log('error', err); next(err); }
});

module.exports = router;
