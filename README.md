## The test interview

### The backend side of application is written by Node.js, Express
### Database - MongoDB
### Caching - Redis
### Cron - node-cron

For run the application need to make .env file with .env.example.
After that run node app.js

How is working ?

Need to call endpoint http://localhost:8000/api/crypto?fsyms=BTC,LINK,MKR&tsyms=USD,EUR
But you can change the query criteria. The backend is caching result from https://min-api.cryptocompare.com/data/pricemultifull to redis.
After 5 minutes try to save results from redis to database crypto collection.
