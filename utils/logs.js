
const winston = require('winston');


const errorLogger = winston.createLogger({
  level: 'silly',
  format: winston.format.combine(
    winston.format.simple(),
    winston.format.colorize(),
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: process.env.ERROR_LOG }),
  ],
});

const infoLogger = winston.createLogger({
  format: winston.format.simple(),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: process.env.ACCESS_LOG }),
  ],
});


function errorLog(e) {
  if (e instanceof Error) {
    return errorLogger.log('error', e.stack);
  }
  return errorLogger.log('error', new Error(e).stack);
}

function infoLog(info) {
  infoLogger.log('info', info);
}

module.exports = {
  errorLog,
  infoLog,
};
