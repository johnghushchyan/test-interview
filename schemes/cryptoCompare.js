/**
 * Module dependencies
 */

const mongoose = require('mongoose');
const { connBase } = require('../connections/db1');

const { Schema } = mongoose;

/**
 * IndividualsLists Schema
 */

const cryptoCompareSchema = new Schema({
  RAW: { type: Object, required: true },
  DISPLAY: { type: Object, required: true },
}, {
  versionKey: false,
});

const CryptoCompareDbModel = connBase.model('CryptoCompare', cryptoCompareSchema, 'cryptoCompare');

module.exports = {
  CryptoCompareDbModel,
};
